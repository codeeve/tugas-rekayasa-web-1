import React from 'react';
import ReactDOM from 'react-dom';

class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {nama:'', nim:'',email:''};

    this.handleChangeNama = this.handleChangeNama.bind(this);
    this.handleChangeNim = this.handleChangeNim.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeNama(event) {
    this.setState({nama: event.target.value });

  }
  handleChangeNim(event) {
    this.setState({nim: event.target.value });

  }
  handleChangeEmail(event) {
    this.setState({email: event.target.value });

  }


  handleSubmit(event) {
    console.log(this.state);
    ReactDOM.render(<div>Nama: {this.state.nama} <br></br>Nim: {this.state.nim} <br></br>Email: {this.state.email}</div>,   document.getElementById('tampil'));
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <h1>FORM KASMIR</h1>
        <label>
          Nama:
          <input type="text" onChange={this.handleChangeNama} />
        </label><br></br>
        <label>
          NIM:
          <input type="text" onChange={this.handleChangeNim} />
        </label><br></br>
        <label>
          Email:
          <input type="text" onChange={this.handleChangeEmail} />
        </label>
        <br></br>
        <input type="submit" value="Submit" />
        <hr></hr>
        <div id="tampil"></div>
      </form>
    );
  }
}


ReactDOM.render(
  <NameForm />,
  document.getElementById('root')
);

export default NameForm;
